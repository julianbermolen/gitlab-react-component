# GitLabRepo
UI Component to show the last user repositories.

# Demo
You can see this on www.julianbermolen.com home page.

# Install
    npm install gitlab-react-component

# Usage

    import GitLabRepo from 'gitlab-react-component'

    const MyComponent = () => {
        return (
            <GitLabRepo user="myusername"/>
        )
    }

    export default MyComponent

# props

user: Your gitlab user's name. It will only answer with your public repositories.